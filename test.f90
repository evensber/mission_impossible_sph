program test

    use kernel, only:get_kernel,rkern

    implicit none

    !
    ! Allocate all the variables
    !
    integer, parameter :: N = 100
    real, dimension(N) :: q, w, wdash

    integer :: id
    do id=1, N
        q(id) = id*(rkern+0.2)/N
        call get_kernel(q(id), w(id), wdash(id))
        print*,q(id),w(id),wdash(id)
    enddo 

 end program test