module equation_of_state
    use constants,only:gamma,isothermal_sound_speed
    implicit none

    character(len=*), parameter :: eos_type='adiabatic'

    contains
    
    subroutine get_equation_of_state(rho, u, c, p)
        real, dimension(:), intent(in) :: rho,u
        real, dimension(:), intent(out) :: c, p

        if (eos_type == 'isothermal') then
            c = isothermal_sound_speed !Get dimensions right
            p = c**2 * rho
        else if (eos_type == 'adiabatic') then
            p = (gamma-1.0)*rho*u
            c = sqrt(gamma*p/rho)
        endif
    end subroutine get_equation_of_state

end module equation_of_state
