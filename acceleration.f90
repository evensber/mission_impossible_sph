module acceleration
    use constants, only:sigma
    use kernel, only:get_kernel
    implicit none

    contains
    
    !
    !Equation 30
    ! 
    subroutine get_accel(r, v, rho, p, cs, h, m, du, N, a)
        real, dimension(:), intent(in) :: r, v, rho, p, cs, h, m
        real, dimension(:), intent(out) :: du
        integer, intent(in) :: N
        real, dimension(:), intent(out) :: a

        integer :: ia, ib
        real :: rhij

        real :: w_a, w_b, wdash_a, wdash_b

        real :: qa_ab, qb_ab
        qa_ab = 0
        qb_ab = 0

        do ia=1,N
            a(ia) = 0.
            du(ia) = 0.
            do ib=1,N
                if (ia /= ib) then
                    rhij = (r(ia) - r(ib)) / abs(r(ia) - r(ib))
                else
                    rhij = 0
                endif

                call get_kernel(abs(r(ia)-r(ib))/h(ia), w_a, wdash_a)
                call get_kernel(abs(r(ia)-r(ib))/h(ib), w_b, wdash_b)

                qa_ab = q1_12(rho(ia), cs(ia), v(ia)-v(ib),  rhij)
                qb_ab = q1_12(rho(ib), cs(ib), v(ia)-v(ib),  rhij)

                a(ia) = a(ia) - m(ib) * (p(ia) + qa_ab) / rho(ia)**2 * sigma / h(ia)**2 * wdash_a * rhij
                a(ia) = a(ia) - m(ib) * (p(ib) + qb_ab) / rho(ib)**2 * sigma / h(ib)**2 * wdash_b * rhij

                du(ia) = du(ia) + m(ib) * (p(ia) + qa_ab) / rho(ia)**2 * (v(ia)-v(ib)) * wdash_a * rhij 

            end do
        end do
    end subroutine get_accel

    !
    ! Artificial viscosity term
    !    
    real function q1_12(rho1, cs1, v12, rhat12)
        real, intent(in) :: rho1, cs1, v12, rhat12
        if (v12 * rhat12 < 0) then
            q1_12 = -0.5 * rho1 * vsig1(cs1, v12, rhat12) * v12 * rhat12
        else
            q1_12 = 0
        endif
    end function q1_12

    !
    ! "Signal" speed?
    !
    real function vsig1(cs1, v12, rhat12)
        real, intent(in) :: cs1, v12, rhat12
        real, parameter :: alpha = 1.0
        real, parameter :: beta = 2.0
        vsig1 = alpha * cs1 - beta * (v12 * rhat12)
    end function vsig1

end module acceleration