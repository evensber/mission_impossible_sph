module setup
    use constants, only : pi, isothermal_sound_speed, gamma
    implicit none

    contains

    !
    ! Set up things
    !
    subroutine get_setup(pos, vel, mass, sml, u, xmin, xmax, N, Nghosts)
        real, dimension(:), intent(out) :: pos, vel
        real, dimension(:), intent(out) :: mass, sml, u
        real, intent(out) :: xmin, xmax
        integer, intent(in) :: N, Nghosts

        !call get_setup_wave(pos, vel, mass, sml, xmin, xmax, N)
        call get_setup_shocktube(pos, vel, mass, sml, u, xmin, xmax, N, Nghosts)
    end subroutine get_setup

    subroutine ghost_update(pos, vel, acc, mass, sml, p, rho, u, xmin, xmax, N, Nghosts)
        real, dimension(:), intent(out) :: pos, vel, acc
        real, dimension(:), intent(out) :: mass, sml, p, rho, u
        real, intent(out) :: xmin, xmax
        integer, intent(in) :: N, Nghosts
        !call ghost_update_periodic(pos, vel, acc, mass, sml, p, rho, u, xmin, xmax, N, Nghosts)
        call ghost_update_fixed(pos, vel, acc, mass, sml, p, rho, u, xmin, xmax, N, Nghosts)
    end subroutine ghost_update

    subroutine get_setup_wave(pos, vel, mass, sml, xmin, xmax, N, Nghosts)
        real, dimension(:), intent(out) :: pos, vel
        real, dimension(:), intent(out) :: mass, sml
        real, intent(out) :: xmin, xmax
        integer, intent(in) :: N, Nghosts
        real, parameter :: k_0 = 2 * pi !Wave number
        real, parameter :: rho_0 = 1

        integer :: i, NN

        NN = N + 2*Nghosts

        xmin = 0
        xmax = 1

        do i=1, NN
            pos(i)  = xmin + (i-.5) * (xmax-xmin) / NN 
        end do

        do i=1, NN
            vel(i) = 1e-4*isothermal_sound_speed*sin(k_0*pos(i))
            mass(i) = rho_0 * (xmax-xmin) / NN
            sml(i) = 1.2 * (xmax-xmin)/NN
        end do
    end subroutine get_setup_wave

    subroutine get_setup_shocktube(pos, vel, mass, sml, u, xmin, xmax, N, Nghosts)
        real, dimension(:), intent(out) :: pos, vel
        real, dimension(:), intent(out) :: mass, sml, u
        real, intent(out) :: xmin, xmax
        integer, intent(in) :: N, Nghosts

        real, parameter :: rho_l = 1
        real, parameter :: rho_r = .125

        real, parameter :: p_l = 1.0
        real, parameter :: p_r = .1

        real, parameter :: spacing_left = .001
        real, parameter :: spacing_right = .008

        integer :: i, NN
        NN = N + 2*Nghosts

        xmin = -0.5
        xmax =  0.5

        do i=1, 9*NN/10
            pos(i)  = (-9 * NN/10 + i - .5) * (spacing_left) 
        enddo
        do i=9*NN/10+1, NN
            pos(i) = 0 + (i-9*NN/10 - .5) * (spacing_right)
        enddo

        do i=1, NN
            if (pos(i) <= 0) then
                vel(i) = 0
                mass(i) = rho_l * spacing_left
                sml(i) = 1.2 * spacing_left
                u(i) = p_l / ((gamma-1) * rho_l)
            else
                vel(i) = 0
                mass(i) = rho_r * spacing_right
                sml(i) = 1.2 * spacing_right
                u(i) = p_r / ((gamma-1) * rho_r)
            endif
        enddo

    end subroutine get_setup_shocktube

    !
    !
    !
    subroutine ghost_update_periodic(pos, vel, acc, mass, sml, p, rho, u, xmin, xmax, N, Nghosts)
        real, dimension(:), intent(out) :: pos, vel, acc
        real, dimension(:), intent(out) :: mass, sml, p, rho, u
        real, intent(in) :: xmin, xmax
        integer, intent(in) :: N, Nghosts

        pos(N + 1          :N +     Nghosts) = xmax + pos(1:Nghosts)
        pos(N + Nghosts + 1:N + 2 * Nghosts) = xmin - xmax + pos(N-Nghosts+1:N)

        vel(N + 1          :N +     Nghosts) = vel(1:Nghosts)
        vel(N + Nghosts + 1:N + 2 * Nghosts) = vel(N-Nghosts+1:N)
        acc(N + 1          :N +     Nghosts) = acc(1:Nghosts)
        acc(N + Nghosts + 1:N + 2 * Nghosts) = acc(N-Nghosts+1:N)
        mass(N + 1          :N +     Nghosts) = mass(1:Nghosts)
        mass(N + Nghosts + 1:N + 2 * Nghosts) = mass(N-Nghosts+1:N)
        sml(N + 1          :N +     Nghosts) = sml(1:Nghosts)
        sml(N + Nghosts + 1:N + 2 * Nghosts) = sml(N-Nghosts+1:N)
        p(N + 1          :N +     Nghosts) = p(1:Nghosts)
        p(N + Nghosts + 1:N + 2 * Nghosts) = p(N-Nghosts+1:N)
        rho(N + 1          :N +     Nghosts) = rho(1:Nghosts)
        rho(N + Nghosts + 1:N + 2 * Nghosts) = rho(N-Nghosts+1:N)
        u(N + 1          :N +     Nghosts) = u(1:Nghosts)
        u(N + Nghosts + 1:N + 2 * Nghosts) = u(N-Nghosts+1:N)
    end subroutine ghost_update_periodic

    !
    !
    !
    subroutine ghost_update_fixed(pos, vel, acc, mass, sml, p, rho, u, xmin, xmax, N, Nghosts)
        real, dimension(:), intent(in) :: pos
        real, dimension(:), intent(inout) :: vel
        real, dimension(:), intent(in) :: acc
        real, dimension(:), intent(in) :: mass, sml, p, rho, u
        real, intent(in) :: xmin, xmax
        integer, intent(in) :: N, Nghosts

        vel(1:Nghosts) = 0
        vel(N+Nghosts+1:N+2*Nghosts) = 0

        !vel(N + 1          :N +     Nghosts) = 0
        !vel(N + Nghosts + 1:N + 2 * Nghosts) = 0
    end subroutine ghost_update_fixed

end module setup