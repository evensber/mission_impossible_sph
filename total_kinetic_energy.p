set terminal png
set   autoscale                        # scale axes automatically
unset log                              # remove any log-scaling
unset label                            # remove any previous labels
set xtic auto                          # set xtics automatically
set ytic auto                          # set ytics automatically
set title "Behaviour as a function of smoothing iterations"
set xlabel "Time"
set ylabel "Total kinetic energy"
plot "total_kinetic_energy00.out" using 2:3 title 's0' with lines, "total_kinetic_energy03.out" using 2:3 title 's3' with lines
