module constants

    implicit none

    integer, parameter :: Nmax = 2000
    real, parameter :: pi = 4.*atan(1.)
    real, parameter :: gamma = 1.4 !Polytropic index
    real, parameter :: isothermal_sound_speed = 1 !Sound speed
    real, parameter :: sigma = 2.0/3.0 ! Sigma
    real, parameter :: hfac = 4.2 


end module constants