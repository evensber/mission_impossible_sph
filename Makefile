FC=gfortran
FFLAGS=-O0 -Wall -Wextra -g -fcheck=all -fdefault-real-8 -fdefault-double-8 -finit-real=zero
SRC=constants.f90 setup.f90 kernel.f90 equation_of_state.f90 acceleration.f90 density.f90 main.f90
OBJ=${SRC:.f90=.o}

all :clean main
	./main

test:
	gfortran -o test kernel.f90 test.f90 && ./test > test.out

movie:
	ffmpeg -i splash_%04d.png -r 10 -vb 50M -bt 100M -vcodec mpeg4 -vf setpts=4.*PTS movie.mp4

energy_plot:
	gnuplot total_kinetic_energy.p > total_kinetic_energy.png
visc_plot:
	gnuplot viscosity_kinetic_energy.p > viscosity_kinetic_energy.png

main: $(OBJ)
	$(FC) $(FFLAGS) -o $@ $(OBJ)

clean:
	rm -f *.o *.mod *.out *.png
	rm -f test main

%.o: %.f90
	$(FC) $(FFLAGS) -o $@ -c $<

