module kernel
    use constants, only: sigma, hfac

    implicit none

    real, parameter :: rkern = 2

    contains

    !
    ! The kernel and its derivative
    !
    subroutine get_kernel(q, w, wdash)
        real, intent(in) :: q
        real, intent(out) :: w, wdash

        if (q<1) then
            w     = sigma * ( 1.0/4.0 * (2 - q)**3       - (1-q)**3)
            wdash = sigma * (-3.0/4.0 * (2 - q)**2 + 3.0 * (1-q)**2)
        elseif (q<2) then
            w     = sigma * ( 1.0/4.0 * (2 - q)**3)
            wdash = sigma * (-3.0/4.0 * (2 - q)**2)
        else
            w     = 0
            wdash = 0
        endif
    end subroutine get_kernel

    !
    ! Calculate smoothing length
    !
    subroutine get_smoothing_length(m, rho, it_max, h)
        real, dimension(:), intent(in) :: m, rho
        integer, intent(in) :: it_max
        real, dimension(:), intent(inout) :: h

        integer :: it
        do it=1, it_max
            ! Note this is for one dimensional only
            ! otherwise h = hfac * (m/rho)**(1/ndim)
            h = hfac * (m / rho) 
        enddo
    end subroutine get_smoothing_length

end module kernel