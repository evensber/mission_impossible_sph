program main

    use constants, only: Nmax
    use density_mod, only:get_density
    use kernel, only: get_smoothing_length
    use equation_of_state, only:get_equation_of_state
    use acceleration, only:get_accel
    use setup, only:get_setup, ghost_update

    implicit none

    !
    ! Allocate all the variables
    !
    real, dimension(Nmax) :: position, velocity, accel
    real, dimension(Nmax) :: mass, smoothing_length, density, internal_energy, delta_internal_energy, pressure, sound_speed
    integer :: it
    character(len=64) :: energy_filename

    real :: xmin, xmax

    integer, parameter :: num_particles = 600
    integer, parameter :: num_ghosts = 20

    integer, parameter :: num_timesteps = 800
    integer, parameter :: save_interval = 1

    integer, parameter :: smoothing_iter=3

    real, parameter :: tmax = .2


    ! real, dimension(num_timesteps+1) :: simulation_time
    ! real, dimension(num_timesteps+1) :: total_mass
    ! real, dimension(num_timesteps+1) :: total_momentum
    ! real, dimension(num_timesteps+1) :: total_kinetic_energy
    ! real, dimension(num_timesteps+1) :: total_internal_energy
    ! real, dimension(num_timesteps+1) :: total_specific_energy

    real :: timestep = 1

    real :: time

    !call get_number_arguments(narg)
    !if (narg >= 1) then
    !   call get_command_argument(1,)
    !else
    !   nmax = 100
    !endif

    !
    ! Main bit
    !
    print*,'Mission Impossible SPH solver'
    print*,'Space allocated for particles:',Nmax
    call get_setup(position, velocity, mass, smoothing_length, internal_energy, xmin, xmax, num_particles, num_ghosts)
    print*,'Actual particles:',num_particles
    !print*,'uu= ',internal_energy(1:10)

    call ghost_update(position, velocity, accel, mass, smoothing_length, pressure, density, &
        &internal_energy, xmin, xmax, num_particles, num_ghosts)
    !print*,'uu2= ',internal_energy(1:10)

    print*,'Ghost particles (on each side):',num_ghosts
    print*,'Number of smoothing_length update iterations', smoothing_iter
    time = 0
        call derivs(position, velocity, accel, mass, smoothing_length, pressure, density, &
            &internal_energy, delta_internal_energy, num_particles, num_ghosts)
   
    !print*,'p=',pressure(1:10)
    !print*,internal_energy(1:10)


    !
    ! Start of stepping
    !
    it = 0
    print*,'Start of time-stepping, t=', time
    do while(time < tmax)

        ! simulation_time(it+1) = it * timestep
        ! total_mass(it+1) = sum(mass)
        ! total_momentum(it+1) = sum(mass * velocity)
        ! total_internal_energy(it+1) = sum(internal_energy)
        ! total_kinetic_energy(it+1) = sum(0.5*mass(1:num_particles)*velocity(1:num_particles)**2)
        ! total_specific_energy(it+1) = 0.5*sum(velocity)**2 + sum(internal_energy)

        if (mod(it, save_interval) == 0) then
            call output(position, velocity, accel, mass, smoothing_length, pressure, density, internal_energy, &
                & num_particles, num_ghosts, it, time)
        endif

        !
        ! Some tests
        !call diagnostic(position, velocity, accel, density)

        timestep = 0.2 * minval(smoothing_length/sound_speed)

        call leapfrog(position, velocity, accel, mass, &
            & smoothing_length, pressure, density, internal_energy, &
            & delta_internal_energy, num_particles, num_ghosts, timestep)
        time = time + timestep
        it = it + 1

    enddo
    print*,'End of time-stepping, t=', time
    print*,'Computed ', it-1, ' time-steps.'

    !
    ! Write kinetic energy
    !
    ! write (energy_filename, "(A20,I0.2,A4)") "total_kinetic_energy", smoothing_iter, ".out"
    ! open(unit=1, file=energy_filename, status='replace', form='formatted')      
    ! do it=0,num_timesteps
    !     write(1,*) it, simulation_time(it+1), &
    !         & total_mass(it+1), &
    !         & total_momentum(it+1), &
    !         & total_internal_energy(it+1), &
    !         & total_kinetic_energy(it+1), &
    !         & total_specific_energy(it+1)
    ! end do
    ! close(1)

    !
    !
    contains

!     ! The Rankine-Hugoniot conditions stipulate rho v, p+ rho v*2, and (rho e * P)*v constant 
!     subroutine diagnostic(r, v, a, rho)
!         real, dimension(:), intent(in) :: r, v, a, rho

!         integer :: i
!         real, dimension(size(r)-1) :: temp

!         call md(density*velocity, temp, i)

!         print*, "RH1", i, r(i), temp(i)
! end subroutine diagnostic

!     subroutine md(x, dx, ml)
!         real, dimension(:), intent(in) :: x
!         real, dimension(size(x)-1), intent(out) :: dx
!         integer, intent(out) :: ml 

!         dx = x(2:size(x)) - x(1:size(x)-1)
!         ml = maxloc(dx, DIM=1)
!     end subroutine md



    !
    ! Step forward in time
    !
    subroutine leapfrog(r, v, a0, m, sml, p, rho, u, du0, N, Nghost, dt)
        real, dimension(:), intent(inout) :: r, v
        real, dimension(:), intent(inout) :: a0
        real, dimension(:), intent(inout) :: m, sml, p, rho
        real, dimension(:), intent(inout) :: u, du0
        integer, intent(in) :: N, Nghost
        real, intent(in) :: dt

        real, dimension(Nmax) :: vs, a1, us, du1

        r = r + dt * v + 0.5 * dt**2 * a0
        vs = v + dt * a0
        us = u + dt * du0

        call derivs(r, vs, a1, m, sml, p, rho, us, du1, N, Nghost)
        v = vs + .5 * dt * (a1 - a0)
        u = us + .5 * dt * (du1 - du0)

        du0 = du1
        a0 = a1
    end subroutine leapfrog

    !
    ! Calculate derived quantities
    !
    subroutine derivs(position, velocity, accel, mass, smoothing_length, pressure, density, &
        &internal_energy, delta_internal_energy, num_particles, num_ghosts)
        real, dimension(:), intent(inout) :: position 
        real, dimension(:), intent(inout) :: velocity
        real, dimension(:), intent(out) :: accel
        real, dimension(:), intent(inout) :: mass
        real, dimension(:), intent(inout) :: smoothing_length
        real, dimension(:), intent(out) :: pressure 
        real, dimension(:), intent(out) :: density
        real, dimension(:), intent(inout) :: internal_energy, delta_internal_energy
        integer, intent(in) :: num_particles, num_ghosts

        ! Compute density from others
        call ghost_update(position, velocity, accel, mass, smoothing_length, pressure, density, &
            &internal_energy, xmin, xmax, num_particles, num_ghosts)

        call get_density(mass, position, smoothing_length, num_particles + 2*num_ghosts, density)

        call ghost_update(position, velocity, accel, mass, smoothing_length, pressure, density, &
            &internal_energy, xmin, xmax, num_particles, num_ghosts)

        ! Compute sound speed and pressure from others
        call get_equation_of_state(density, internal_energy, sound_speed, pressure)
        ! Update smoothing length from others (note: inout)
        call get_smoothing_length(mass, density, smoothing_iter, smoothing_length)

    
        ! Compute accel and update internal_energy from others
        call get_accel(position, velocity, density, pressure, sound_speed, &
            &smoothing_length, mass, delta_internal_energy, num_particles + 2*num_ghosts, accel)

        call ghost_update(position, velocity, accel, mass, smoothing_length, pressure, density, &
            &internal_energy, xmin, xmax, num_particles, num_ghosts)

    end subroutine derivs


    !
    ! Print output
    !
    subroutine output(pos, vel, accel, mass, sml, p, rho, u, N, Nghosts, it, time)
        real, dimension(:), intent(in) :: pos, vel, accel
        real, dimension(:), intent(in) :: mass, sml, p, rho, u
        integer, intent(in) :: N, Nghosts, it
        real, intent(in) :: time

        integer :: i 

        character(len=64) :: filename
        filename = 'results.out'

        write (filename, "(A7,I0.3,A4)") "results", it, ".out"

        open(unit=1, file=filename, status='replace', form='formatted')        
        write(1,*) time
        do i=1, N+2*Nghosts
            write(1,*) i, pos(i), vel(i), accel(i), mass(i), sml(i), p(i), rho(i) , u(i)
        end do
        close(1)

!        write(*,"(I0.3,A2)",advance='no') it, ", "
    
    end subroutine output

end program main