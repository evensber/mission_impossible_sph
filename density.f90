module density_mod
    use kernel, only:get_kernel, rkern

    implicit none

    contains

    !
    ! Return density at each particle
    !
    subroutine get_density(m, r, h, N, rho)
        real, dimension(:), intent(in) :: m, r, h
        integer, intent(in) :: N
        real, dimension(:), intent(out) :: rho

        integer :: ia, ib
        real :: q, w, wdash

        do ia=1,N
            rho(ia) = 0
            do ib=1,N
                q = abs(r(ia) - r(ib)) / h(ia)
                if (q<rkern) then
                    call get_kernel(q, w, wdash)
                    rho(ia) = rho(ia) + m(ib) / h(ia) * w
                end if
            end do
        end do

    end subroutine get_density

end module density_mod